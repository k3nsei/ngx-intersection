/*
 * Public API Surface of infinite-scroll
 */

export * from './lib/infinite-scroll.service';
export * from './lib/infinite-scroll.component';
export * from './lib/infinite-scroll.module';
