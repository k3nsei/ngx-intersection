/*
 * Public API Surface of lazy-images
 */

export * from './lib/lazy-images.service';
export * from './lib/lazy-images.component';
export * from './lib/lazy-images.module';
