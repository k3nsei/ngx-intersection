import { TestBed, inject } from '@angular/core/testing';

import { LazyImagesService } from './lazy-images.service';

describe('LazyImagesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LazyImagesService]
    });
  });

  it('should be created', inject([LazyImagesService], (service: LazyImagesService) => {
    expect(service).toBeTruthy();
  }));
});
