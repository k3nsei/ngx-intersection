import { NgModule } from '@angular/core';
import { LazyImagesComponent } from './lazy-images.component';

@NgModule({
  imports: [
  ],
  declarations: [LazyImagesComponent],
  exports: [LazyImagesComponent]
})
export class LazyImagesModule { }
