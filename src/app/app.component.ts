import { Component } from '@angular/core';

@Component({
  selector: 'intxn-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'intxn';
}
